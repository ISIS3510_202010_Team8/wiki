# [Wiki](https://gitlab.com/ISIS3510_202010_TeamCoure/wiki/-/wikis/home)

# Contract

Group work agreement

Agreement of group _8 - Team Coure_


To improve and facilitate the experience of working in groups, it is important to discuss and negotiate the rules, expectations and responsibilities that each member of the team will have in the project. The idea of this document is that all members of the team know what to expect from each other, what rights do they have inside the group, the tools, rules and agreements that the group is going to use for communication, planning and conflict mediation and lastly how they should face specific situations that are common in group work.

1.	Each member of the team is responsible for:
<br/>
●	Cumplimiento de tareas, previamente discutidas y establecidas, dentro del margen de tiempo establecido. Si no es posible el cumplimiento de alguna tarea, hacérselo saber al grupo con anticipación. 
<br/>
●	Manejo del tiempo y organización del mismo, teniendo en cuenta la carga académica del semestre que implican demás materias cursadas. 
<br/>
●	Proporcionar un trato respetuoso y digno a todas las personas del equipo
<br/>
●	Presentar de forma oportuna y respetuosa ideas o reclamos respecto al trabajo realizado
<br/>
●	Sugerir mejoras a las actividades que se realicen e informar sus inquietudes a los demás miembros del equipo
<br/>
●	Asistir puntual y continuamente a las reuniones y encuentros programados
<br/>
●	Brindar información precisa, oportuna, completa, veraz y actualizada sobre las tareas que le son asignadas
<br/>

2.	Each member of the team has the right to:
<br/>
●	 Ser tratado con el respeto y la consideración debida a la dignidad de la persona
<br/>
●	Presentar ideas de manera verbal, escrita, o por cualquier otro medio idóneo, así como a obtener información y orientación acerca de los requisitos que estas ideas requieran
<br/>
●	Conocer el estado de cualquier petición o idea presentada
<br/>
●	Exigir el cumplimiento de las responsabilidades de los demás miembros del equipo
<br/>
●	Recibir oficialmente las disculpas de los miembros del equipo en caso de presentarse errores cometidos 
<br/>
●	Solicitar y obtener información de manera precisa, oportuna, completa, veraz y actualizada, acerca de las tareas del proyecto
<br/>

3.	Rules and agreements
<br/>
●	The rules and agreements for communication under which we will work are:
<br/>
o	Comunicación a través de un grupo privado por WhatsApp
<br/>
o	10:00 PM se establece como hora límite para comunicarse por medio de WhatsApp. Con excepción de posibles emergencias (e.g., pérdida o daño de algún entregable) 
<br/>
o	Establecer espacios fuera de clase para discutir, opinar, organizar los respectivos entregables
<br/>
o	Cumplir con las fechas designadas por el grupo para realizar las tareas que le son asignadas
<br/>
o	Mantener informado siempre al resto del equipo sobre las actividades que cada miembro está realizando.
<br/>
o	En el caso de reuniones extraordinarias se hará uso de un servidor de Discord el cual estará enlazado mediante webhook al servidor del proyecto
<br/>
o	Registrar por WhatsApp y Todoist todas las decisiones tomadas en persona por el grupo.
<br/>

●	The rules and agreements for planning under which we will work are:
<br/>
o	Seguimiento de tareas por medio de Todoist 
<br/>
o	Reuniones de retroalimentación al final de cada entrega
<br/>
o	Notificar, adicionalmente, del cumplimiento de actividades en Todoist por WhatsApp.
<br/>

●	The rules and agreements for conflict mediation under which we will work are:

<br/>
o	Establecer un espacio en el cual se pueda efectuar una comunicación adecuada, oportuna y directa frente a los problemas que puedan surgir, preferiblemente de manera presencial 
<br/>
o	Involucrar a todo el equipo en pro de obtener una perspectiva más variada y objetiva
<br/>
o	Finalizar cada reunión con una posible solución y un plazo de implementación para la misma
<br/>
o	Toda decisión tomada debe quedar registrada en el grupo privado de WhatsApp y la plataforma de Todoist.
<br/>

4.	How is the team going to face the following situations?

<br/>
●	A team member is not meeting the expectations that where agreed upon in this document. How is the rest of the team going to deal with the situation? (State the communication strategies you are going to use, and the consequences could be for the team member.)

Se establecerá una reunión grupal en la que se tratarán los siguientes puntos:
<br/>
1.	¿Que ha hecho bien hasta ahora?
<br/>
2.	¿Qué no ha hecho bien?
<br/>
3.	¿Cómo estos errores afectan al resto del equipo?
<br/>
4.	¿Cuáles son las soluciones que propone para el problema?
<br/>
5.	¿Cuáles son las soluciones que el resto del equipo propone para el problema?
<br/>
6.	Se establecerá una solución que funcione para todos y un lapso de tiempo para corregir los errores causados.
<br/>
●	There has been a disagreement in between some team members, what are the mediation steps the team is going to follow. If you’ve done everything that is listed below, and the conflict is not solved what is going to be the last step. At what moment are you going to look for outside help to solve the disagreement. 
<br/>
Se acordará una reunión con cada miembro del grupo que se ha visto afectado y se discutirán las siguientes preguntas:
<br/>
1.	¿Qué originó el conflicto?

<br/>
2.	¿Sientes que tu trabajo se ve afectado por este conflicto? ¿De qué manera?
<br/>
3.	¿Sientes que tu relación interpersonal con la otra persona se ve afectada por este conflicto? ¿De qué manera?
<br/>
4.	¿Estás genuinamente interesado en arreglar el conflicto? ¿Crees que es un trabajo de los dos hacerlo o sólo de alguna de las partes? ¿Por qué?
<br/>
5.	¿Qué debería hacer la otra persona para hacerte sentir mejor al respecto?
<br/>
6.	¿Qué crees que deberías hacer tú para hacer sentir mejor a la otra persona?
<br/>

●	A team member is behind schedule with their work. How should the team member act? When should he do it? How is the rest of the team going to react? What are going to be the consequences for not following these actions? What are the consequences for being late with their work?
<br/>
Lo que debe pasar en esta situación es:
<br/>
1.	El miembro del equipo que se encuentra atrasado, debe intentar notificar a todo el equipo sobre su estado
<br/>
2.	Esta notificación debe ocurrir a más tardar 2 días antes de las fechas de entrega.
<br/>
3.	Los otros miembros del equipo disponibles, deben intentar completar la mayor cantidad de actividades faltantes del miembro atrasado.
<br/>
4.	Como consecuencia el miembro del grupo debe comprometerse a cumplir en la próxima entrega y de acuerdo al sistema de calificación del curso, una disminución de la nota consecuente al retraso del miembro.
<br/>
●	One or more team members want to leave the group. What is going to happen with the idea (Who is going to keep it, are you both going to continue developing the idea, etc) What is going to happen with the code and the current products you have developed?
	<br/>
Lo que sucederá en este caso es:
<br/>
1.	El código, productos, documentos y demás entregables desarrollados como grupo pertenecen al grupo.
<br/>
2.	La persona que desee dejar el grupo y luego de esto quiera hacer uso del código entregado debe solicitar el permiso correspondiente al grupo para este fin.

<br/>
5.	Is there any other situation you think must be described in this agreement, state them below and explain the actions the team should take.  

<br/>
●	Uno o más estudiantes del grupo retiran la materia. ¿Cómo se realizará la redistribución de requerimientos y  participación de cada integrante en el desarrollo de las aplicaciones?
<br/>
Para esta situación:
<br/>
1.	Se realizará una reunión con los miembros restantes del grupo con el fin de reasignar funciones para el desarrollo del curso.
<br/>
2.	Esta reasignación tomará como principio la siguiente distribución:
<br/>
a.	Un integrante dedicado netamente al desarrollo de android y otro al de IOS.
<br/>
b.	Un integrante que desarrollará y brindará apoyo en los dos productos mencionados en el literal anterior.
<br/>
3.	Para los trabajos de documentos se repartirán los ítems de manera equitativa entre los integrantes.

<br/>
Signatures:







<br/>

Name: Paula Andrea Velandia Ramos  
<br/>
Code:   201715353  
<br/>
<br/>
Name: Juan Felipe Parra Camargo
<br/>
Code: 201714259
     <br/>
     <br/>


Name: Andrés Felipe Rodriguez Murillo 
  <br/>
Code:  2017169                                                                      
  <br/>
     <br/>

 Name: Sebastian García
   <br/>
  Code: 201630047